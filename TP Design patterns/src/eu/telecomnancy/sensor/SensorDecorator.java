package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.SensorObs;

public class SensorDecorator implements ISensor {

	ISensor sensor;
	
	public SensorDecorator(ISensor sensor) {
		this.sensor=sensor;
	}
	
	@Override
	public void on() {
		sensor.on();
	}

	@Override
	public void off() {
		sensor.off();
	}

	@Override
	public boolean getStatus() {
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		sensor.update();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return sensor.getValue();
	}

	@Override
	public void attach(SensorObs observer) {
		sensor.attach(observer);
	}

	@Override
	public void detach(SensorObs observer) {
		sensor.detach(observer);
	}

	@Override
	public void notifyObs() {
		sensor.notifyObs();
	}
}
