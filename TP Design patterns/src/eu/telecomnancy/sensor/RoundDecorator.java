package eu.telecomnancy.sensor;
 
public class RoundDecorator extends SensorDecorator {
	
	public RoundDecorator(ISensor sensor) {
		super(sensor);
	}
	
	public double getValue() throws SensorNotActivatedException {
		return Math.round(sensor.getValue());
	}
}
