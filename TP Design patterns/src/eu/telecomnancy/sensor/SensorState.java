package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.SensorObs;

public abstract class SensorState implements ISensor {

	@Override
	public void on() {
	}

	@Override
	public void off() {
	}

	@Override
	public boolean getStatus() {
		return false;
	}

	@Override
	public void update() throws SensorNotActivatedException {
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return 0;
	}

	@Override
	public void attach(SensorObs observer) {
	}

	@Override
	public void detach(SensorObs observer) {
	}

	@Override
	public void notifyObs() {
	}

}
