package eu.telecomnancy.sensor;

import java.util.ArrayList;
import java.util.Collection;

import eu.telecomnancy.ui.SensorObs;

public class SensorAdapter implements ISensor {
	
	private LegacyTemperatureSensor LTS;
	private Collection<SensorObs> observers;
	
	private int fahrenheit = 0;
	
	public SensorAdapter(LegacyTemperatureSensor LTS) {
		this.LTS=LTS;
		this.observers=new ArrayList<SensorObs>();
	}

	@Override
	public void on() {
		if (!getStatus()) { // le sensor est eteint
			LTS.onOff();
		}
		else {
			return;
		}
	}

	@Override
	public void off() {
		if (getStatus()) { // le sensor est allume
			LTS.onOff();
		}
		else {
			return;
		}
	}

	@Override
	public boolean getStatus() {
		return(LTS.getStatus());
	}

	@Override
	public void update() throws SensorNotActivatedException {
		return;
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return(LTS.getTemperature());
	}

	@Override
	public void notifyObs() {
		for (SensorObs obs : observers) {
			obs.update();
		}
	}

	@Override
	public void attach(SensorObs observer) {
		observers.add(observer);
		
	}

	@Override
	public void detach(SensorObs observer) {
		observers.remove(observer);
	}

	public int getFahrenheit() {
		return fahrenheit;
	}

	public void setFahrenheit(int fahrenheit) {
		this.fahrenheit = fahrenheit;
	}
	
}
