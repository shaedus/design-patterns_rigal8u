package eu.telecomnancy.sensor;

public class ConvertDecorator extends SensorDecorator {

	public ConvertDecorator(ISensor sensor) {
		super(sensor);
	}
	
	public double getValue() throws SensorNotActivatedException {
		double r = sensor.getValue();
		r=r*(1/0.556)+32;
		return r;
	}
	
}
