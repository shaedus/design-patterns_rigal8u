package eu.telecomnancy.sensor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

import eu.telecomnancy.ui.SensorObs;

public class TemperatureSensor extends SensorState {
    private boolean state;
    private double value = 0;
    
    private int fahrenheit;
    
    Collection<SensorObs> observers;
    boolean subjectState;
    
    public TemperatureSensor() {
    	this.state=false;
    	this.value=0;
    	this.observers=new ArrayList<SensorObs>();
    }

    @Override
    public void on() {
        state = true;
    }

    @Override
    public void off() {
        state = false;
    }

    @Override
    public boolean getStatus() {
        return state;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (state){
            value = (new Random()).nextDouble() * 100;
        	notifyObs();
        }
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (state){
            return value;
        }
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

	@Override
	public void attach(SensorObs observer) {
		observers.add(observer);
	}

	@Override
	public void detach(SensorObs observer) {
		observers.remove(observer);
	}

	@Override
	public void notifyObs() {
		for (SensorObs obs : observers) {
			obs.update();
		}
	}
	
	public boolean getState() {
		return subjectState;
	}
	
	public void setState(boolean state) {
		this.state=state;
	}

	public int getFahrenheit() {
		return fahrenheit;
	}

	public void setFahrenheit(int fahrenheit) {
		this.fahrenheit = fahrenheit;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public boolean isSubjectState() {
		return subjectState;
	}

	public void setSubjectState(boolean subjectState) {
		this.subjectState = subjectState;
	}

}
