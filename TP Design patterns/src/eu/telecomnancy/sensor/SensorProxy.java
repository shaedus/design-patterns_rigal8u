package eu.telecomnancy.sensor;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import eu.telecomnancy.ui.SensorObs;

public class SensorProxy extends SensorState {
	
	private ISensor sensor = new TemperatureSensor();
	private PrintWriter writer = null;
	
	//Constructeur
	public SensorProxy(ISensor sensor) {
		this.sensor=sensor;
		try {
			writer = new PrintWriter("log.txt", "UTF-8");
			writer.println("LOGS ----- Creation : "+java.time.LocalDateTime.now());
			writer.println("");
			writer.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.out.println("LOG file created, logs are printed in the file and not in console.");
		System.out.println("It should be at the root of the project under then name 'log.txt' (UTF-8 encoded)");
}
	
	
	@Override
    public void on() {
		sensor.on();
		writer.println("["+java.time.LocalDateTime.now()+"] -- "+"Sensor has been switched on");
		writer.flush();
	}

    @Override
    public void off() {
        sensor.off();
		writer.println("["+java.time.LocalDateTime.now()+"] -- "+"Sensor has been switched off");
		writer.flush();
    }

    @Override
    public boolean getStatus() {
		writer.println("["+java.time.LocalDateTime.now()+"] -- "+"State asked, returned : "+sensor.getStatus());
		writer.flush();
		return sensor.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        sensor.update();
		writer.println("["+java.time.LocalDateTime.now()+"] -- "+"Sensor updated");
		writer.flush();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
		writer.println("["+java.time.LocalDateTime.now()+"] -- "+"Value asked, returned "+sensor.getValue());
		writer.flush();
		return sensor.getValue();
    }

	@Override
	public void attach(SensorObs observer) {
		sensor.attach(observer);
	}

	@Override
	public void detach(SensorObs observer) {
		sensor.detach(observer);
	}

	@Override
	public void notifyObs() {
		sensor.notifyObs();
	}
	
}