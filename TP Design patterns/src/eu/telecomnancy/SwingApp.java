package eu.telecomnancy;

import eu.telecomnancy.sensor.ConvertDecorator;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.RoundDecorator;
import eu.telecomnancy.sensor.SensorAdapter;
import eu.telecomnancy.sensor.SensorProxy;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
    	// Not used anymore but works
    	//LegacyTemperatureSensor LTS = new LegacyTemperatureSensor();
        //ISensor sensor = new SensorAdapter(LTS);
    	
    	// Sensor creation
    	ISensor sensor = new TemperatureSensor();
    	// Decorators, currently both are used but you can chose to use only one or none
    	// First decorator
    	ConvertDecorator sensorConverted = new ConvertDecorator(sensor);
    	// Second decorator
    	RoundDecorator sensorRounded = new RoundDecorator(sensorConverted);
    	// Sensor decorated
    	ISensor sensorDecorated = sensorRounded;
    	// Proxy, LOGS
    	SensorProxy proxy = new SensorProxy(sensorDecorated);
    	// Window creation
        new MainWindow(proxy);
    }

}
